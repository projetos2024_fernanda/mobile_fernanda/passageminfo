import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({ navigation }) => {
  const tasks = [
    {
      id: 1,
      title: "Ir ao Mercado",
      data: "2024-02-27",
      time: "10:00",
      address: "Supermercado SS",
    },
    {
      id: 2,
      title: "Fazer exercícios",
      data: "2024-02-27",
      time: "15:30",
      address: "Academia FitLife",
    },
    {
      id: 3,
      title: "Ler livro",
      data: "2024-02-28",
      time: "18:00",
      address: "Biblioteca Municipal",
    },
  ];


    const taskPress =(task) =>{
        navigation.navigate('DetalhesDasTarefas',{task})
        //passa parametro da task 
    } 


  return (
    <View>
      <FlatList
      data={tasks} // Dá onde vem os dados
      keyExtractor={(item) => item.id.toString()} //Como identicar cada item (id) para identificar nesse caso como String
      renderItem={({ item }) => ( //reinderizar cada item que for puxado e retornar
       <TouchableOpacity onPress={() => taskPress(item)}>   
        <Text>{item.title}</Text>
       </TouchableOpacity>
    // chama a const que rediciona para a outra tela quando pressionado
    // chama a função onPress que chama a taskPress
      )}
      />
    </View>
  );
};

export default TaskList;
